# vscode_settings

My VS Code settings, so I can drop it into different developer environments

### Quick commands:

Pull existing VS Code settings:

    cp $HOME/.config/Code/User/*.json ./
    
Generate install file for existing VS Code extensions:

    code --list-extensions | xargs -L 1 echo code --install-extension > ./extensions
    
Install settings from this repo into VS Code:

    cp ./*.json $HOME/.config/Code/User/
    
    
# Reasons for settings change

## settings.json

* `"editor.multiCursorModifier": "ctrlCmd",` - Sets multicursors on ctrl+click
* `"javascript.preferences.importModuleSpecifier": "non-relative",` - auto-import used absolute links instead of relative - MAY CHANGE DEPENDING ON ENVIRONMENT
* `"editor.suggestSelection": "recentlyUsed",` - prefer recently used auto-imports over first found in Intellisense
* `"vsintellicode.modify.editor.suggestSelection": "automaticallyOverrodeDefaultValue",` - indicates to the editor that we've manually set the suggestSelection field
    *  related: https://github.com/MicrosoftDocs/intellicode/issues/78#issuecomment-477694255
* `"editor.formatOnSave": true` - uses Prettier extension to format code on save

## keybindings.json

* `ctrl+t, extension.transpose` - swaps two highlighted blocks/lines/text

# Extensions

* Rainbow Brackets
* HTMLTagWraps - press alt+w to wrap a selected block of code in new tag
* VS Code ESLint
* JSON Tools - helps writing custom json
* Prettier - handles formatting code styles according to presets
* VS Code Docker
* VS Code Remote - allows connecting to a remote workspace
* Debugger for Chrome
* VS Code React Native
* Sublime Commands - handler for the transpose key mapping